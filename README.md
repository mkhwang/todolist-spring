![](https://i1.wp.com/technology.amis.nl/wp-content/uploads/2018/01/spring-boot.png?w=702&ssl=1)

### TODO-LIST API (Java-Spring)


----------
- 개발환경
    - OS : Mac OS
    - IDE : Intellij
    - Spring :
        - version : 2.0.4.RELEASE
        - Boot
        - JPA(Hibernate) & QueryDSL
    - Database : Mysql
    - Documentation : /swagger-ui.html
    - Tomcat
        - version : Tomcat 9.0.8
    - 세부설정 application.yml 참고
- 인증
    - JWT
        - IN : Header
        - Key : X-TodoList-Authorization
        - AccessToken : expire 1 month
        - RefreshToken : expire 3 month        
        - Route : /auth/login