package com.mkhwang.todolist.core.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QBaseAudit is a Querydsl query type for BaseAudit
 */
@Generated("com.querydsl.codegen.SupertypeSerializer")
public class QBaseAudit extends EntityPathBase<BaseAudit> {

    private static final long serialVersionUID = 1173604403L;

    public static final QBaseAudit baseAudit = new QBaseAudit("baseAudit");

    public final DateTimePath<java.time.ZonedDateTime> createdAt = createDateTime("createdAt", java.time.ZonedDateTime.class);

    public final NumberPath<Long> idx = createNumber("idx", Long.class);

    public final DateTimePath<java.time.ZonedDateTime> updatedAt = createDateTime("updatedAt", java.time.ZonedDateTime.class);

    public QBaseAudit(String variable) {
        super(BaseAudit.class, forVariable(variable));
    }

    public QBaseAudit(Path<? extends BaseAudit> path) {
        super(path.getType(), path.getMetadata());
    }

    public QBaseAudit(PathMetadata metadata) {
        super(BaseAudit.class, metadata);
    }

}

