package com.mkhwang.todolist.core.domain.task;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QTask is a Querydsl query type for Task
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QTask extends EntityPathBase<Task> {

    private static final long serialVersionUID = 1148436791L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QTask task = new QTask("task");

    public final com.mkhwang.todolist.core.domain.QBaseAudit _super = new com.mkhwang.todolist.core.domain.QBaseAudit(this);

    //inherited
    public final DateTimePath<java.time.ZonedDateTime> createdAt = _super.createdAt;

    public final DatePath<java.time.LocalDate> deadlineAt = createDate("deadlineAt", java.time.LocalDate.class);

    public final StringPath detail = createString("detail");

    public final DateTimePath<java.time.ZonedDateTime> finishedAt = createDateTime("finishedAt", java.time.ZonedDateTime.class);

    //inherited
    public final NumberPath<Long> idx = _super.idx;

    public final BooleanPath isFinished = createBoolean("isFinished");

    public final EnumPath<com.mkhwang.todolist.core.domain.task.type.TaskPriority> priority = createEnum("priority", com.mkhwang.todolist.core.domain.task.type.TaskPriority.class);

    public final DatePath<java.time.LocalDate> startAt = createDate("startAt", java.time.LocalDate.class);

    public final StringPath title = createString("title");

    //inherited
    public final DateTimePath<java.time.ZonedDateTime> updatedAt = _super.updatedAt;

    public final com.mkhwang.todolist.core.domain.user.QUser user;

    public QTask(String variable) {
        this(Task.class, forVariable(variable), INITS);
    }

    public QTask(Path<? extends Task> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QTask(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QTask(PathMetadata metadata, PathInits inits) {
        this(Task.class, metadata, inits);
    }

    public QTask(Class<? extends Task> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.user = inits.isInitialized("user") ? new com.mkhwang.todolist.core.domain.user.QUser(forProperty("user")) : null;
    }

}

