package com.mkhwang.todolist.core.domain.user;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QUser is a Querydsl query type for User
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QUser extends EntityPathBase<User> {

    private static final long serialVersionUID = 1307098359L;

    public static final QUser user = new QUser("user");

    public final com.mkhwang.todolist.core.domain.QBaseAudit _super = new com.mkhwang.todolist.core.domain.QBaseAudit(this);

    public final DateTimePath<java.time.ZonedDateTime> closedAt = createDateTime("closedAt", java.time.ZonedDateTime.class);

    //inherited
    public final DateTimePath<java.time.ZonedDateTime> createdAt = _super.createdAt;

    //inherited
    public final NumberPath<Long> idx = _super.idx;

    public final BooleanPath isClosed = createBoolean("isClosed");

    public final DateTimePath<java.time.ZonedDateTime> loginAt = createDateTime("loginAt", java.time.ZonedDateTime.class);

    public final DateTimePath<java.time.ZonedDateTime> logoutAt = createDateTime("logoutAt", java.time.ZonedDateTime.class);

    public final StringPath mail = createString("mail");

    public final StringPath password = createString("password");

    public final EnumPath<com.mkhwang.todolist.core.domain.user.type.UserRole> role = createEnum("role", com.mkhwang.todolist.core.domain.user.type.UserRole.class);

    public final ListPath<com.mkhwang.todolist.core.domain.task.Task, com.mkhwang.todolist.core.domain.task.QTask> tasks = this.<com.mkhwang.todolist.core.domain.task.Task, com.mkhwang.todolist.core.domain.task.QTask>createList("tasks", com.mkhwang.todolist.core.domain.task.Task.class, com.mkhwang.todolist.core.domain.task.QTask.class, PathInits.DIRECT2);

    //inherited
    public final DateTimePath<java.time.ZonedDateTime> updatedAt = _super.updatedAt;

    public final StringPath userId = createString("userId");

    public QUser(String variable) {
        super(User.class, forVariable(variable));
    }

    public QUser(Path<? extends User> path) {
        super(path.getType(), path.getMetadata());
    }

    public QUser(PathMetadata metadata) {
        super(User.class, metadata);
    }

}

