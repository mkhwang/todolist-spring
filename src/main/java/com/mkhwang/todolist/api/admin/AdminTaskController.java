package com.mkhwang.todolist.api.admin;

import com.mkhwang.todolist.core.domain.task.Task;
import com.mkhwang.todolist.core.domain.task.type.TaskPriority;
import com.mkhwang.todolist.core.response.TodoListPageResponse;
import com.mkhwang.todolist.core.service.task.TaskService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

/**
 * Created by mkhwang on 2018. 8. 3.
 */
@Secured("ROLE_ADMIN")
@RestController
@RequestMapping("/admin/tasks")
@Api(value = "사용자 관련", description = "관리자 권한 필요", tags = "관리자")
public class AdminTaskController {
    @Autowired
    private TaskService taskService;

    @GetMapping
    @ApiOperation(value = "TodoList 검색",
            notes = "TodoList 검색",
            authorizations = {@Authorization(value = "JWT")})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startDate", dataType = "string", paramType = "query", required = true, value = "시작일(yyyy-MM-dd)"),
            @ApiImplicitParam(name = "endDate", dataType = "string", paramType = "query", required = true, value = "종료일(yyyy-MM-dd)"),
            @ApiImplicitParam(name = "finish", dataType = "integer", paramType = "query", value = "0:전체/1:미종료/2:종료"),
            @ApiImplicitParam(name = "priority", dataType = "string", paramType = "query", value = "LOW/MIDDLE/HIGH"),
            @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query",
                    value = "페이지 1 ~ N"),
            @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query",
                    value = "페이지당 아이템 수"),
            @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query",
                    value = "항목 별 정렬 : 항목(,asc|desc). " +
                            "default : priority,desc " +
                            "복수 가능. ex) sort=idx,asc&sort=idx,desc")
    })
    public TodoListPageResponse getTasks(
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate startDate,
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endDate,
            @RequestParam(value = "keyword", required = false, defaultValue = "") String keyword,
            @RequestParam(value = "priority", required = false, defaultValue = "") TaskPriority taskPriority,
            @RequestParam(value = "finish", required = false, defaultValue = "0") int finish,
            @PageableDefault(sort = {"priority"}, direction = Sort.Direction.DESC, size = 10, page = 1) Pageable pageable
    ) {
        Task searchTask = new Task();
        searchTask.setTitle(keyword);
        searchTask.setSearchFinishType(finish);
        searchTask.setPriority(taskPriority);
        searchTask.setStartAt(startDate);
        searchTask.setDeadlineAt(endDate);
        return TodoListPageResponse.ok(taskService.searchTask(0L, searchTask, pageable));
    }
}
