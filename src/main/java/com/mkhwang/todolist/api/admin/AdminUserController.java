package com.mkhwang.todolist.api.admin;

import com.mkhwang.todolist.core.domain.user.User;
import com.mkhwang.todolist.core.domain.user.type.UserRole;
import com.mkhwang.todolist.core.response.TodoListPageResponse;
import com.mkhwang.todolist.core.response.TodoListResponse;
import com.mkhwang.todolist.core.service.user.UserService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

/**
 * Created by mkhwang on 2018. 8. 3.
 */
@Secured("ROLE_ADMIN")
@RestController
@RequestMapping("/admin/users")
@Api(value = "사용자 관련", description = "관리자 권한 필요", tags = "관리자")
public class AdminUserController {
    @Autowired
    private UserService userService;

    @GetMapping
    @ApiOperation(value = "사용자 검색",
            notes = "사용자 검색",
            authorizations = {@Authorization(value = "JWT")})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "keyword", dataType = "string", paramType = "query",
                    value = "사용자 ID or E-mail"),
            @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query",
                    value = "페이지 1 ~ N"),
            @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query",
                    value = "페이지당 아이템 수"),
            @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query",
                    value = "항목 별 정렬 : 항목(,asc|desc). " +
                            "default : idx,desc " +
                            "복수 가능. ex) sort=idx,asc&sort=idx,desc")
    })
    public TodoListPageResponse getUsers(
            @RequestParam(value = "keyword", required = false, defaultValue = "") String keyword,
            @PageableDefault(sort = {"idx"}, direction = Sort.Direction.DESC, size = 10, page = 1) Pageable pageable
    ) {
        User searchUser = new User();
        searchUser.setUserId(keyword);
        searchUser.setMail(keyword);
        Page<User> result = userService.searchUsers(searchUser, pageable);
        return TodoListPageResponse.ok(result);
    }

    @PutMapping("/{idx}")
    @ApiOperation(value = "사용자 권한 수정",
            notes = "사용자 권한 수정",
            authorizations = {@Authorization(value = "JWT")})
    public TodoListResponse modifyUser(@PathVariable Long userIdx, @RequestBody UserRole body) {
        User requestUser = new User();
        requestUser.setIdx(userIdx);
        requestUser.setRole(body);
        userService.modifyUser(requestUser);
        return TodoListResponse.ok(null);
    }
}
