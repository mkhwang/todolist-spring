package com.mkhwang.todolist.api.auth;

import com.mkhwang.todolist.core.domain.TodoToken;
import com.mkhwang.todolist.core.domain.user.User;
import com.mkhwang.todolist.core.domain.user.UserAuthority;
import com.mkhwang.todolist.core.request.auth.RequestUserJoin;
import com.mkhwang.todolist.core.request.auth.RequestUserLogin;
import com.mkhwang.todolist.core.response.TodoListResponse;
import com.mkhwang.todolist.core.service.auth.AuthService;
import com.mkhwang.todolist.core.service.user.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * Created by mkhwang on 2018. 8. 3.
 */
@RestController
@RequestMapping("/auth")
@Api(value = "인증관련", description = "인증", tags = "인증")
public class AuthController {
    @Autowired
    private UserService userService;

    @Autowired
    private AuthService authService;

    @PostMapping("/login")
    @ApiOperation(value = "ID/PW를 이용하여 로그인",
            notes = "ID/PW를 이용하여 로그인")
    public TodoListResponse login(@Validated @RequestBody RequestUserLogin body) {
        User requestUser = new User();
        requestUser.setUserId(body.getUserId());
        requestUser.setPassword(body.getPassword());
        TodoToken todoToken = null;
        if (userService.loginUser(requestUser)) {
            todoToken = authService.generateToken(requestUser);
        }
        return TodoListResponse.ok(todoToken);
    }

    @PostMapping("/join")
    @ApiOperation(value = "회원가입",
            notes = "회원가입")
    public TodoListResponse join(@Validated @RequestBody RequestUserJoin body) {
        User user = new User();
        user.setUserId(body.getUserId());
        user.setPassword(body.getPassword());
        user.setMail(body.getMail());
        return TodoListResponse.created(userService.addUser(user));
    }

    @Secured("IS_AUTHENTICATED_ANONYMOUSLY")
    @PostMapping("/logout")
    @ApiOperation(value = "로그아웃",
            notes = "로그아웃",
            authorizations = {@Authorization(value = "JWT")})
    public TodoListResponse logOut() {
        UserAuthority userAuthority = (UserAuthority) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        userService.logoutUser(userAuthority.getIdx());
        return TodoListResponse.ok();
    }

    @GetMapping("/user/{userId}")
    @ApiOperation(value = "사용자 아이디 중복 체크",
            notes = "사용자 아이디 중복 체크")
    public TodoListResponse checkIdDuplicated(@PathVariable String userId) {
        return TodoListResponse.ok(userService.isUserIdDuplicated(userId));
    }
}
