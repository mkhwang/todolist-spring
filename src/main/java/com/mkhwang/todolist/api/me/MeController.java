package com.mkhwang.todolist.api.me;

import com.mkhwang.todolist.core.domain.user.User;
import com.mkhwang.todolist.core.domain.user.UserAuthority;
import com.mkhwang.todolist.core.request.auth.RequestModifyUser;
import com.mkhwang.todolist.core.response.TodoListResponse;
import com.mkhwang.todolist.core.service.user.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

/**
 * Created by mkhwang on 2018. 8. 3.
 */
@Secured("IS_AUTHENTICATED_ANONYMOUSLY")
@RestController
@RequestMapping("/me")
@Api(value = "내 정보 관련", description = "내 정보 관련", tags = "내 정보")
public class MeController {
    @Autowired
    private UserService userService;

    @GetMapping
    @ApiOperation(value = "내 정보 확인",
            notes = "내 정보 확인",
            authorizations = {@Authorization(value = "JWT")})
    public TodoListResponse findMe() {
        UserAuthority userAuthority = (UserAuthority) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return TodoListResponse.ok(userService.findUserByUserIdx(userAuthority.getIdx()));
    }

    @PutMapping
    @ApiOperation(value = "내 정보 수정",
            notes = "내 정보 수정",
            authorizations = {@Authorization(value = "JWT")})
    public TodoListResponse modifyMe(@RequestBody RequestModifyUser body) {
        UserAuthority userAuthority = (UserAuthority) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User requestUser = new User();
        requestUser.setMail(body.getMail());
        requestUser.setPassword(body.getPassword());
        requestUser.setIdx(userAuthority.getIdx());
        return TodoListResponse.modify(userService.modifyUser(requestUser));
    }

    @DeleteMapping
    @ApiOperation(value = "회원 탈퇴",
            notes = "회원 탈퇴",
            authorizations = {@Authorization(value = "JWT")})
    public TodoListResponse deleteMe() {
        UserAuthority userAuthority = (UserAuthority) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return TodoListResponse.modify(userService.deleteUser(userAuthority.getIdx()));
    }
}
