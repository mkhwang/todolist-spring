package com.mkhwang.todolist.api.task;

import com.mkhwang.todolist.core.domain.task.Task;
import com.mkhwang.todolist.core.domain.task.type.TaskPriority;
import com.mkhwang.todolist.core.domain.user.UserAuthority;
import com.mkhwang.todolist.core.request.task.RequestTaskAdd;
import com.mkhwang.todolist.core.response.TodoListPageResponse;
import com.mkhwang.todolist.core.response.TodoListResponse;
import com.mkhwang.todolist.core.service.task.TaskService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

/**
 * Created by mkhwang on 2018. 8. 3.
 */
@Secured("IS_AUTHENTICATED_ANONYMOUSLY")
@RestController
@RequestMapping("/tasks")
@Api(value = "TodoList 관련", description = "업무", tags = "업무")
public class TaskController {
    @Autowired
    private TaskService taskService;

    @GetMapping
    @ApiOperation(value = "TodoList 검색",
            notes = "TodoList 검색",
            authorizations = {@Authorization(value = "JWT")})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startDate", dataType = "string", paramType = "query", required = true, value = "시작일(yyyy-MM-dd)"),
            @ApiImplicitParam(name = "endDate", dataType = "string", paramType = "query", required = true, value = "종료일(yyyy-MM-dd)"),
            @ApiImplicitParam(name = "priority", dataType = "string", paramType = "query", value = "LOW/MIDDLE/HIGH"),
            @ApiImplicitParam(name = "finish", dataType = "integer", paramType = "query", value = "0:전체/1:미종료/2:종료"),
            @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query",
                    value = "페이지 1 ~ N"),
            @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query",
                    value = "페이지당 아이템 수"),
            @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query",
                    value = "항목 별 정렬 : 항목(,asc|desc). " +
                            "default : sort=createdAt,desc " +
                            "복수 가능. ex) sort=idx,asc&sort=idx,desc")
    })
    public TodoListPageResponse getTasks(
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate startDate,
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endDate,
            @RequestParam(value = "keyword", required = false, defaultValue = "") String keyword,
            @RequestParam(value = "priority", required = false, defaultValue = "") TaskPriority taskPriority,
            @RequestParam(value = "finish", required = false, defaultValue = "0") int finish,
            @PageableDefault(sort = {"createdAt"}, direction = Sort.Direction.DESC, size = 10, page = 1) Pageable pageable
    ) {
        UserAuthority userAuthority = (UserAuthority) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Task searchTask = new Task();
        searchTask.setTitle(keyword);
        searchTask.setSearchFinishType(finish);
        searchTask.setPriority(taskPriority);
        searchTask.setStartAt(startDate);
        searchTask.setDeadlineAt(endDate);
        return TodoListPageResponse.ok(taskService.searchTask(userAuthority.getIdx(), searchTask, pageable));
    }

    @GetMapping("/byDays")
    @ApiOperation(value = "TodoList 요일별 검색",
            notes = "TodoList 요일별 검색",
            authorizations = {@Authorization(value = "JWT")})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startDate", dataType = "string", paramType = "query", required = true, value = "시작일(yyyy-MM-dd)"),
            @ApiImplicitParam(name = "endDate", dataType = "string", paramType = "query", required = true, value = "종료일(yyyy-MM-dd)"),
            @ApiImplicitParam(name = "finish", dataType = "integer", paramType = "query", value = "0:전체/1:미종료/2:종료"),
            @ApiImplicitParam(name = "priority", dataType = "string", paramType = "query", value = "LOW/MIDDLE/HIGH"),
            @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query",
                    value = "페이지 1 ~ N"),
            @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query",
                    value = "페이지당 아이템 수"),
            @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query",
                    value = "항목 별 정렬 : 항목(,asc|desc). " +
                            "default : idx.asc\n" +
                            "복수 가능. ex) sort=idx,asc&sort=idx,desc")
    })
    public TodoListPageResponse getTasksByDays(
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate startDate,
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endDate,
            @RequestParam(value = "keyword", required = false, defaultValue = "") String keyword,
            @RequestParam(value = "priority", required = false, defaultValue = "") TaskPriority taskPriority,
            @RequestParam(value = "finish", required = false, defaultValue = "0") int finish,
            @PageableDefault(sort = {"createdAt"}, direction = Sort.Direction.DESC, size = 10, page = 1) Pageable pageable
    ) {
        return TodoListPageResponse.ok(null);
    }

    @PostMapping
    @ApiOperation(value = "TodoList 추가",
            notes = "TodoList 추가",
            authorizations = {@Authorization(value = "JWT")})
    public TodoListResponse addTask(@Validated @RequestBody RequestTaskAdd body) {
        System.out.println(body.toString());
        UserAuthority userAuthority = (UserAuthority) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Task task = new Task(body);
        return TodoListResponse.created(taskService.addTask(userAuthority.getIdx(), task));
    }

    @PutMapping("/{taskIdx}")
    @ApiOperation(value = "TodoList 수정",
            notes = "TodoList 수정",
            authorizations = {@Authorization(value = "JWT")})
    public TodoListResponse modifyTask(@Validated @PathVariable Long taskIdx, @RequestBody RequestTaskAdd body) {
        UserAuthority userAuthority = (UserAuthority) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Task task = new Task(body);
        task.setIdx(taskIdx);
        return TodoListResponse.modify(taskService.modifyTask(userAuthority.getIdx(), task));
    }

    @PutMapping("/{taskIdx}/finish")
    @ApiOperation(value = "TodoList 완료(toggle)",
            notes = "TodoList 완료(toggle)",
            authorizations = {@Authorization(value = "JWT")})
    public TodoListResponse finishTask(@Validated @PathVariable Long taskIdx) {
        UserAuthority userAuthority = (UserAuthority) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return TodoListResponse.modify(taskService.toggleTask(userAuthority.getIdx(), taskIdx));
    }

    @DeleteMapping("/{taskIdx}")
    @ApiOperation(value = "TodoList 삭제",
            notes = "TodoList 삭제",
            authorizations = {@Authorization(value = "JWT")})
    public TodoListResponse deleteTask(@Validated @PathVariable Long taskIdx) {
        UserAuthority userAuthority = (UserAuthority) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return TodoListResponse.modify(taskService.deleteTask(userAuthority.getIdx(), taskIdx));
    }
}
