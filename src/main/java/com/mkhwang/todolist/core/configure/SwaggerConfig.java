package com.mkhwang.todolist.core.configure;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by mkhwang on 2018. 8. 3.
 */
@Configuration
@EnableAutoConfiguration
@ComponentScan
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.mkhwang"))
                .paths(PathSelectors.any())
                .build()
                .consumes(getAllConsumeContentTypes())
                .produces(gettAllProduceContextTypes())
                .apiInfo(appInfo())
                .securitySchemes(Collections.singletonList(apiKey()))
                .ignoredParameterTypes(Pageable.class, PagedResourcesAssembler.class);
    }

    private ApiInfo appInfo() {
        return new ApiInfo("TodoList REST API",
                "TodoList REST API",
                "1.0",
                "",
                new Contact("MinKi Hnwag", "", "hmk6264@gmail.com"),
                "Copyright(C) 2018. All right reserved by MinKi Hwang", "", Collections.emptyList());
    }

    private Set<String> gettAllProduceContextTypes() {
        Set<String> produces = new HashSet<>();
        produces.add("application/json");
        return produces;
    }

    private Set<String> getAllConsumeContentTypes() {
        Set<String> consumes = new HashSet<>();
        consumes.add("application/json");
        return consumes;
    }

    private ApiKey apiKey() {
        return new ApiKey("JWT", "X-TodoList-Authorization", "header");
    }
}
