package com.mkhwang.todolist.core.configure.security;

import com.mkhwang.todolist.core.domain.user.UserAuthority;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by mkhwang on 2018. 8. 3.
 */
@Component
public class JwtTokenProvider {
    private static final Logger logger = LoggerFactory.getLogger(JwtTokenProvider.class);

    @Value("${app.jwtSecret}")
    private String jwtSecret;

    @Value("${app.jwtExpirationInMs}")
    private Long jwtExpirationInMs;

    public boolean validateToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(jwtSecret.getBytes(Charset.forName("UTF8"))).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException ex) {
            logger.error("Invalid JWT signature");
        } catch (MalformedJwtException ex) {
            logger.error("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            logger.error("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            logger.error("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            logger.error("JWT claims string is empty.");
        }
        return false;
    }

    public UsernamePasswordAuthenticationToken getAuthentication(String token) {
        if (token != null) {
            Claims claims = getTokenClaims(token);

            if(!claims.containsKey("role"))
                return null;

            UserAuthority authority = new UserAuthority(Long.parseLong(claims.getSubject()), claims.get("role").toString());
            List<GrantedAuthority> authorities = new ArrayList<>();
            authorities.add(new SimpleGrantedAuthority((String) claims.get("role")));
            return new UsernamePasswordAuthenticationToken(authority, null, authorities);
        }
        return null;
    }

    public Claims getTokenClaims(String token) {
        return Jwts.parser()
                .setSigningKey(jwtSecret.getBytes(Charset.forName("UTF8")))
                .parseClaimsJws(token)
                .getBody();
    }

    public String generateAccessToken(Long userIdx, String role) {
        Date now = new Date();
        Date expiryDate = new Date(now.getTime() + jwtExpirationInMs);
        return Jwts.builder()
                .setSubject(Long.toString(userIdx))
                .claim("role", role)
                .claim("type", "access")
                .setIssuedAt(now)
                .setExpiration(expiryDate)
                .setHeaderParam("typ", "JWT")
                .signWith(SignatureAlgorithm.HS256, jwtSecret.getBytes(Charset.forName("UTF8")))
                .compact();
    }

    public String generateRefreshToken(Long userIdx, String role) {
        Date now = new Date();
        Date expiryDate = new Date(now.getTime() + jwtExpirationInMs * 3);

        return Jwts.builder()
                .setSubject(Long.toString(userIdx))
                .claim("role", role)
                .claim("type", "refresh")
                .setIssuedAt(now)
                .setExpiration(expiryDate)
                .setHeaderParam("typ", "JWT")
                .signWith(SignatureAlgorithm.HS256, jwtSecret.getBytes(Charset.forName("UTF8")))
                .compact();
    }

    public boolean isAccessToken(String token) {
        Claims claims = getTokenClaims(token);
        return "access".equals(claims.get("type").toString());
    }

}
