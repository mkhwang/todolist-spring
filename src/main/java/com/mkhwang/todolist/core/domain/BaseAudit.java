package com.mkhwang.todolist.core.domain;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

/**
 * Created by mkhwang on 2018. 8. 3.
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@DynamicInsert
@DynamicUpdate
@Data
@Getter
@Setter
public abstract class BaseAudit implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    protected Long idx;

    @CreatedDate
    @Column(name = "created_at", nullable = false, updatable = false)
    protected ZonedDateTime createdAt;

    @LastModifiedDate
    @Column(name = "updated_at", nullable = false)
    protected ZonedDateTime updatedAt;

    @PrePersist
    protected void onPersist() {
        this.createdAt = this.updatedAt = getTimeStamp();
    }

    @PreUpdate
    protected void onUpdate() {
        this.updatedAt = getTimeStamp();
    }

    private ZonedDateTime getTimeStamp() {
        return Instant.now().atZone(ZoneId.of("Asia/Seoul"));
    }
}
