package com.mkhwang.todolist.core.domain;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by mkhwang on 2018. 8. 3.
 */
@Data
@Getter
@Setter
public class TodoToken {
    private String accessToken;
    private String refreshToken;
    private String tokenType = "Bearer";
    private String role;
}
