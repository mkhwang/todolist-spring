package com.mkhwang.todolist.core.domain.task;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.mkhwang.todolist.core.domain.BaseAudit;
import com.mkhwang.todolist.core.domain.task.type.TaskPriority;
import com.mkhwang.todolist.core.domain.user.User;
import com.mkhwang.todolist.core.request.task.RequestTaskAdd;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;

/**
 * Created by mkhwang on 2018. 8. 3.
 */
@Entity
@Table(name = "task")
@Getter
@Setter
public class Task extends BaseAudit {
    @Column(name = "title")
    private String title;

    @Column(name = "detail")
    private String detail;

    @Enumerated(EnumType.STRING)
    @Column(name = "priority")
    private TaskPriority priority;

    @Column(name = "is_finished")
    private boolean isFinished;

    @Column(name = "start_at")
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate startAt;

    @Column(name = "deadline_at")
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate deadlineAt;

    @Column(name = "finished_at")
    private ZonedDateTime finishedAt;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_idx", foreignKey = @ForeignKey(name = "FK_task_user_idx"), nullable = false)
    private User user;

    @JsonIgnore
    @Transient
    private int searchFinishType;

    public Task() {
    }

    public Task(RequestTaskAdd requestTaskAdd) {
        this.title = requestTaskAdd.getTitle();
        this.detail = requestTaskAdd.getDetail();
        this.priority = requestTaskAdd.getPriority();
        this.deadlineAt = requestTaskAdd.getDeadlineAt();
        this.startAt = requestTaskAdd.getStartAt();
        this.isFinished = false;
    }

    @PreUpdate
    private void checkFinished() {
        if (isFinished)
            this.finishedAt = Instant.now().atZone(ZoneId.of("Asia/Seoul"));
        else
            this.finishedAt = null;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("userId")
    public String getUserId() {
        return this.user == null ? null : this.user.getUserId();
    }
}
