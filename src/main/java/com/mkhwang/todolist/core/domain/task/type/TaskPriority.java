package com.mkhwang.todolist.core.domain.task.type;

/**
 * Created by mkhwang on 2018. 8. 3.
 */
public enum TaskPriority {
    LOW, MIDDLE, HIGH
}
