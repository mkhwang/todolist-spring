package com.mkhwang.todolist.core.domain.user;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.mkhwang.todolist.core.domain.BaseAudit;
import com.mkhwang.todolist.core.domain.task.Task;
import com.mkhwang.todolist.core.domain.user.type.UserRole;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.List;

/**
 * Created by mkhwang on 2018. 8. 3.
 */
@Entity
@Table(name = "user")
@Getter
@Setter
public class User extends BaseAudit {
    @Column(name = "userId", unique = true)
    private String userId;

    @Column(name = "password")
    private String password;

    @Column(name = "mail")
    private String mail;

    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    private UserRole role;

    @Column(name = "login_at")
    private ZonedDateTime loginAt;

    @Column(name = "login_out_at")
    private ZonedDateTime logoutAt;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonManagedReference
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Task> tasks;

    @Column(name = "is_closed")
    private boolean isClosed;

    @Column(name = "closed_at")
    private ZonedDateTime closedAt;
}
