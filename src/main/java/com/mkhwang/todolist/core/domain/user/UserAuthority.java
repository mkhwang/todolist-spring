package com.mkhwang.todolist.core.domain.user;

import com.mkhwang.todolist.core.domain.user.type.UserRole;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by mkhwang on 2018. 8. 5.
 */
@Data
@Getter
@Setter
public class UserAuthority {
    private Long idx;
    private UserRole userRole;

    public UserAuthority(Long idx, String userRole) {
        this.idx = idx;
        if(userRole.equals("ROLE_ADMIN"))
            this.userRole = UserRole.ROLE_ADMIN;
        else
            this.userRole = UserRole.ROLE_USER;
    }
}
