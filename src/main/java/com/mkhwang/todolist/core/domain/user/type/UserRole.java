package com.mkhwang.todolist.core.domain.user.type;

/**
 * Created by mkhwang on 2018. 8. 3.
 */
public enum UserRole {
    ROLE_USER, ROLE_ADMIN
}
