package com.mkhwang.todolist.core.repository.task;

import com.mkhwang.todolist.core.domain.task.Task;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by mkhwang on 2018. 8. 6.
 */
public interface CustomTaskRepository {
    Page<Task> searchTask(Long userIdx, Task task, Pageable pageable);
}
