package com.mkhwang.todolist.core.repository.task;

import com.mkhwang.todolist.core.domain.task.Task;
import com.mkhwang.todolist.core.domain.user.User;
import com.mkhwang.todolist.core.repository.user.CustomUserRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by mkhwang on 2018. 8. 6.
 */
@Repository
public interface TaskRepository extends PagingAndSortingRepository<Task, Long>, CustomTaskRepository {
    Task findTaskByIdxAndUser(Long idx, User user);

    List<Task> findTasksByUser(User user);
}
