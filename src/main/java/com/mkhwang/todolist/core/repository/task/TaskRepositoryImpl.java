package com.mkhwang.todolist.core.repository.task;

import com.mkhwang.todolist.core.domain.task.QTask;
import com.mkhwang.todolist.core.domain.task.Task;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.QueryResults;
import com.querydsl.jpa.JPQLQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

/**
 * Created by mkhwang on 2018. 8. 6.
 */
@Repository
public class TaskRepositoryImpl extends QuerydslRepositorySupport implements CustomTaskRepository {
    public TaskRepositoryImpl() {
        super(Task.class);
    }

    @Override
    public Page<Task> searchTask(Long userIdx, Task task, Pageable pageable) {
        QTask qTask = QTask.task;
        JPQLQuery query = from(qTask);
        if (userIdx > 0)
            query.where(qTask.user.idx.eq(userIdx));
        if (StringUtils.hasText(task.getTitle())) {
            if (task.getTitle().contains(" ")) {
                BooleanBuilder builder = new BooleanBuilder();
                for (String keyword : task.getTitle().split(" ")) {
                    builder.or(qTask.title.like("%" + keyword + "%"))
                            .or(qTask.detail.like("%" + keyword + "%"));
                }
                query.where(builder);
            } else {
                query.where(
                        qTask.title.like("%" + task.getTitle() + "%")
                                .or(qTask.detail.like("%" + task.getTitle() + "%"))
                );
            }
        }
        if (task.getPriority() != null)
            query.where(qTask.priority.eq(task.getPriority()));

        if (task.getSearchFinishType() == 2)
            query.where(qTask.isFinished.isFalse());
        else if (task.getSearchFinishType() == 1)
            query.where(qTask.isFinished.isTrue());

        query.where(qTask.startAt.goe(task.getStartAt()))
                .where(qTask.startAt.loe(task.getDeadlineAt()))
                .where(qTask.deadlineAt.goe(task.getStartAt()))
                .where(qTask.deadlineAt.loe(task.getDeadlineAt()));

        QueryResults queryResults = getQuerydsl().applyPagination(pageable, query).fetchResults();
        return new PageImpl<Task>(queryResults.getResults(), pageable, queryResults.getTotal());
    }
}
