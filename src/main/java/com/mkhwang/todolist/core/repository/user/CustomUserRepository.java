package com.mkhwang.todolist.core.repository.user;

import com.mkhwang.todolist.core.domain.user.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by mkhwang on 2018. 8. 3.
 */
public interface CustomUserRepository {
    Page<User> searchUser(User searchUser, Pageable pageable);
}
