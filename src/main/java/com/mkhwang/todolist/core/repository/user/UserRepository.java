package com.mkhwang.todolist.core.repository.user;

import com.mkhwang.todolist.core.domain.user.User;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by mkhwang on 2018. 8. 3.
 */
@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long>, CustomUserRepository {
    User findUserByUserId(String userId);
}
