package com.mkhwang.todolist.core.repository.user;

import com.mkhwang.todolist.core.domain.user.QUser;
import com.mkhwang.todolist.core.domain.user.User;
import com.querydsl.core.QueryResults;
import com.querydsl.jpa.JPQLQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

/**
 * Created by mkhwang on 2018. 8. 3.
 */
@Repository
public class UserRepositoryImpl extends QuerydslRepositorySupport implements CustomUserRepository {
    public UserRepositoryImpl() {
        super(User.class);
    }

    @Override
    public Page<User> searchUser(User searchUser, Pageable pageable) {
        QUser qUser = QUser.user;
        JPQLQuery query = from(qUser);

        if(StringUtils.hasText(searchUser.getUserId()))
            query.where(qUser.userId.like("%" + searchUser.getUserId() + "%")
                .or(qUser.mail.like("%" + searchUser.getUserId() + "%")));
        if(searchUser.getRole() != null)
            query.where(qUser.role.eq(searchUser.getRole()));
        QueryResults queryResults = getQuerydsl().applyPagination(pageable, query).fetchResults();
        return new PageImpl<User>(queryResults.getResults(), pageable, queryResults.getTotal());
    }
}
