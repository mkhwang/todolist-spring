package com.mkhwang.todolist.core.request.auth;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by mkhwang on 2018. 8. 6.
 */
@Data
@Getter
@Setter
public class RequestModifyUser {
    private String mail;
    private String password;
}
