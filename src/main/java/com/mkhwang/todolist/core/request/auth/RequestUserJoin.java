package com.mkhwang.todolist.core.request.auth;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;


/**
 * Created by mkhwang on 2018. 8. 3.
 */
@Data
@Getter
@Setter
public class RequestUserJoin {
    @NotNull(message = "userId is null")
    private String userId;
    @NotNull(message = "password is null")
    private String password;
    @NotNull(message = "mail is null")
    private String mail;
}
