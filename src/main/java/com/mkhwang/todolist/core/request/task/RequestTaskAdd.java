package com.mkhwang.todolist.core.request.task;

import com.mkhwang.todolist.core.domain.task.type.TaskPriority;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * Created by mkhwang on 2018. 8. 6.
 */
@Data
@Getter
@Setter
public class RequestTaskAdd {
    @NotNull(message = "title is null")
    private String title;
    @NotNull(message = "detail is null")
    private String detail;
    @NotNull(message = "priority is null")
    @ApiModelProperty(notes = "LOW | MIDDLE | HIGH \nMIDDLE is default", required = true)
    private TaskPriority priority;
    @NotNull(message = "startAt is null")
    private LocalDate startAt;
    @NotNull(message = "deadlineAt is null")
    private LocalDate deadlineAt;
}
