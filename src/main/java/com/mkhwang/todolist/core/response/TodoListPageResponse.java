package com.mkhwang.todolist.core.response;

import com.mkhwang.todolist.core.response.page.TodoListPage;
import lombok.Data;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

/**
 * Created by mkhwang on 2018. 8. 13.
 */
@Data
public class TodoListPageResponse {
    private final Object data;
    private final ZonedDateTime timeStamp;
    private final HttpStatus responseCode;
    private final TodoListPage pageInfo;

    private TodoListPageResponse(HttpStatus responseCode, Page data) {
        if (data != null) {
            this.data = data.getContent();
            this.pageInfo = new TodoListPage(data);
        } else {
            this.data = this.pageInfo = null;
        }
        this.timeStamp = Instant.now().atZone(ZoneId.of("Asia/Seoul"));
        this.responseCode = responseCode;
    }

    private static TodoListPageResponse of(HttpStatus responseCode, Page data) {
        return new TodoListPageResponse(responseCode, data);
    }

    public static TodoListPageResponse ok(Page data) {
        if (data == null || data.getContent().size() == 0)
            return of(HttpStatus.NOT_FOUND, null);
        return of(HttpStatus.ACCEPTED, data);
    }
}
