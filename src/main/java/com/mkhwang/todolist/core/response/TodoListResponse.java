package com.mkhwang.todolist.core.response;

import lombok.Data;
import org.springframework.http.HttpStatus;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Optional;

/**
 * Created by mkhwang on 2018. 8. 3.
 */
@Data
public class TodoListResponse {
    private final Object data;
    private final ZonedDateTime timeStamp;
    private final HttpStatus responseCode;

    private TodoListResponse(HttpStatus responseCode, Object data) {
        this.data = data;
        this.timeStamp = Instant.now().atZone(ZoneId.of("Asia/Seoul"));
        this.responseCode = responseCode;
    }

    private static TodoListResponse of(HttpStatus responseCode, Object data) {
        return new TodoListResponse(responseCode, data);
    }

    public static TodoListResponse ok(Object data) {
        if (data == null || data == Optional.empty())
            return of(HttpStatus.NOT_FOUND, null);
        return of(HttpStatus.ACCEPTED, data);
    }

    public static TodoListResponse ok() {
        return of(HttpStatus.OK, null);
    }

    public static TodoListResponse created(Object data) {
        return of(HttpStatus.CREATED, data);
    }

    public static TodoListResponse modify(Object data) {
        return of(HttpStatus.NO_CONTENT, data);
    }

    public static TodoListResponse notFound() {
        return of(HttpStatus.NOT_FOUND, null);
    }
}
