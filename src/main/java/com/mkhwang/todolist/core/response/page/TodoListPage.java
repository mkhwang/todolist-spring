package com.mkhwang.todolist.core.response.page;

import lombok.Data;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.springframework.data.domain.Sort.*;

/**
 * Created by mkhwang on 2018. 8. 13.
 */
@Data
public class TodoListPage {
    private int pageSize;
    private int pageNumber;
    private boolean last;
    private boolean first;
    private int totalPages;
    private int totalElements;
    private List<TodoListSort> sort;

    public TodoListPage(Page page) {
        this.pageSize = page.getPageable().getPageSize();
        this.pageNumber = page.getPageable().getPageNumber() + 1;
        this.last = page.isLast();
        this.first = page.isFirst();
        this.totalPages = page.getTotalPages();
        this.totalElements = page.getNumberOfElements();
        this.sort = getSortString(page.getSort());
    }

    private List<TodoListSort> getSortString(Sort sort) {
        List<TodoListSort> result = new ArrayList<>();
        Iterator sortIterator = sort.iterator();
        while (sortIterator.hasNext()) {
            Object object = sortIterator.next();
            if (object instanceof Order){
                Sort.Order order = (Order) object;
                result.add(new TodoListSort(order.getProperty(), order.getDirection()));
            }
        }
        return result;
    }
}
