package com.mkhwang.todolist.core.response.page;

import lombok.Data;
import org.springframework.data.domain.Sort;

/**
 * Created by mkhwang on 2018. 8. 13.
 */
@Data
public class TodoListSort {
    private String property;
    private Sort.Direction direction;

    public TodoListSort(String property, Sort.Direction direction) {
        this.property = property;
        this.direction = direction;
    }
}
