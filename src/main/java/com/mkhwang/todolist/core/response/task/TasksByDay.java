package com.mkhwang.todolist.core.response.task;

import com.mkhwang.todolist.core.domain.task.Task;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mkhwang on 2018. 8. 3.
 */
@Data
@Getter
@Setter
public class TasksByDay {
    private List<Task> sun;
    private List<Task> mon;
    private List<Task> tue;
    private List<Task> wed;
    private List<Task> thu;
    private List<Task> fri;
    private List<Task> sat;

    public void addSunTask(Task task) {
        if(this.sun == null)
            sun = new ArrayList<>();
        sun.add(task);
    }

    public void addMonTask(Task task) {
        if(this.mon == null)
            mon = new ArrayList<>();
        mon.add(task);
    }

    public void addTueTask(Task task) {
        if(this.tue == null)
            tue = new ArrayList<>();
        tue.add(task);
    }

    public void addWedTask(Task task) {
        if(this.wed == null)
            wed = new ArrayList<>();
        wed.add(task);
    }

    public void addThuTask(Task task) {
        if(this.thu == null)
            thu = new ArrayList<>();
        thu.add(task);
    }

    public void addFriTask(Task task) {
        if(this.fri == null)
            fri = new ArrayList<>();
        fri.add(task);
    }

    public void addSatTask(Task task) {
        if(this.sat == null)
            sat = new ArrayList<>();
        sat.add(task);
    }
}
