package com.mkhwang.todolist.core.service.auth;

import com.mkhwang.todolist.core.domain.TodoToken;
import com.mkhwang.todolist.core.domain.user.User;

/**
 * Created by mkhwang on 2018. 8. 3.
 */
public interface AuthService {
    TodoToken generateToken(User user);
    TodoToken generateTokenUsingRefreshToken(String refreshToken);
}
