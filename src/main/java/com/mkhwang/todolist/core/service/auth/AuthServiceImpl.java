package com.mkhwang.todolist.core.service.auth;

import com.mkhwang.todolist.core.configure.security.JwtTokenProvider;
import com.mkhwang.todolist.core.domain.TodoToken;
import com.mkhwang.todolist.core.domain.user.User;
import com.mkhwang.todolist.core.domain.user.type.UserRole;
import com.mkhwang.todolist.core.repository.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * Created by mkhwang on 2018. 8. 3.
 */
@Service
public class AuthServiceImpl implements AuthService {
    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public TodoToken generateToken(User user) {
        TodoToken todoToken = null;
        User findUser = userRepository.findUserByUserId(user.getUserId());
        if (findUser != null && bCryptPasswordEncoder.matches(user.getPassword(), findUser.getPassword())) {
            String role = "";
            if (findUser.getRole().equals(UserRole.ROLE_USER))
                role = "ROLE_USER";
            else
                role = "ROLE_ADMIN";
            todoToken = new TodoToken();
            todoToken.setAccessToken(jwtTokenProvider.generateAccessToken(findUser.getIdx(), role));
            todoToken.setRefreshToken(jwtTokenProvider.generateRefreshToken(findUser.getIdx(), role));
            todoToken.setRole(role);
        }
        return todoToken;
    }

    @Override
    public TodoToken generateTokenUsingRefreshToken(String refreshToken) {
        //@Todo : implementation
        return null;
    }
}
