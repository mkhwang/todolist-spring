package com.mkhwang.todolist.core.service.task;

import com.mkhwang.todolist.core.domain.task.Task;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by mkhwang on 2018. 8. 6.
 */
public interface TaskService {
    Task addTask(Long userIdx, Task task);

    boolean modifyTask(Long userIdx, Task task);

    boolean deleteTask(Long userIdx, Long taskIdx);

    Page<Task> searchTask(Long userIdx, Task task, Pageable pageable);

    boolean toggleTask(Long userIdx, Long taskIdx);
}
