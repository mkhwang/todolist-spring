package com.mkhwang.todolist.core.service.task;

import com.mkhwang.todolist.core.domain.task.Task;
import com.mkhwang.todolist.core.domain.task.type.TaskPriority;
import com.mkhwang.todolist.core.domain.user.User;
import com.mkhwang.todolist.core.repository.task.TaskRepository;
import com.mkhwang.todolist.core.repository.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Optional;

/**
 * Created by mkhwang on 2018. 8. 6.
 */
@Service
public class TaskServiceImpl implements TaskService {
    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional
    public Task addTask(Long userIdx, Task task) {
        Optional<User> result = userRepository.findById(userIdx);
        if (result.isPresent()) {
            User findUser = result.get();
            if (task.getPriority() == null)
                task.setPriority(TaskPriority.MIDDLE);
            task.setFinished(false);
            task.setUser(findUser);
            taskRepository.save(task);
        }
        return task;
    }

    @Override
    @Transactional
    public boolean modifyTask(Long userIdx, Task task) {
        boolean result = false;
        Optional<User> queryResult = userRepository.findById(userIdx);
        if (queryResult.isPresent()) {
            User findUser = queryResult.get();
            Task findTask = taskRepository.findTaskByIdxAndUser(task.getIdx(), findUser);
            if (findTask != null) {
                if (StringUtils.hasText(task.getTitle()))
                    findTask.setTitle(task.getTitle());
                if (StringUtils.hasText(task.getDetail()))
                    findTask.setDetail(task.getDetail());
                if (task.getPriority() != null)
                    findTask.setPriority(task.getPriority());
                if (task.getStartAt() != null)
                    findTask.setStartAt(task.getStartAt());
                if (task.getFinishedAt() != null)
                    findTask.setFinishedAt(task.getFinishedAt());
                findTask.setFinished(task.isFinished());
                taskRepository.save(findTask);
                result = true;
            }
        }
        return result;
    }

    @Override
    @Transactional
    public boolean deleteTask(Long userIdx, Long taskIdx) {
        boolean result = false;
        Optional<User> queryResult = userRepository.findById(userIdx);
        if (queryResult.isPresent()) {
            User findUser = queryResult.get();
            Task findTask = taskRepository.findTaskByIdxAndUser(taskIdx, findUser);
            if (findTask != null) {
                taskRepository.delete(findTask);
                result = true;
            }
        }
        return result;
    }

    @Override
    @Transactional
    public Page<Task> searchTask(Long userIdx, Task task, Pageable pageable) {
        Page<Task> result = taskRepository.searchTask(userIdx, task, PageRequest.of(pageable.getPageNumber() - 1, pageable.getPageSize(), pageable.getSort()));
        for(Task resultTask : result.getContent()) {
            resultTask.getUser().getUserId();
        }
        return result;
    }

    @Override
    @Transactional
    public boolean toggleTask(Long userIdx, Long taskIdx) {
        boolean result = false;
        Optional<User> queryResult = userRepository.findById(userIdx);
        if (queryResult.isPresent()) {
            User findUser = queryResult.get();
            Task findTask = taskRepository.findTaskByIdxAndUser(taskIdx, findUser);
            if (findTask != null) {
                findTask.setFinished(!findTask.isFinished());
                taskRepository.save(findTask);
                result = true;
            }
        }
        return result;
    }
}
