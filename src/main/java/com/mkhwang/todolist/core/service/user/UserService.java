package com.mkhwang.todolist.core.service.user;

import com.mkhwang.todolist.core.domain.user.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by mkhwang on 2018. 8. 3.
 */
public interface UserService {
    User addUser(User user);

    boolean logoutUser(Long userIdx);

    boolean loginUser(User user);

    boolean modifyUser(User user);

    Page<User> searchUsers(User searchUser, Pageable pageable);

    boolean deleteUser(Long userIdx);

    boolean isUserIdDuplicated(String userId);

    User findUserByUserIdx(Long userIdx);
}
