package com.mkhwang.todolist.core.service.user;

import com.mkhwang.todolist.core.domain.task.Task;
import com.mkhwang.todolist.core.domain.user.User;
import com.mkhwang.todolist.core.domain.user.type.UserRole;
import com.mkhwang.todolist.core.exception.BadRequestException;
import com.mkhwang.todolist.core.repository.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Created by mkhwang on 2018. 8. 3.
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    @Transactional
    public User addUser(User user) {
        user.setRole(UserRole.ROLE_USER);
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setClosed(false);
        return userRepository.save(user);
    }

    @Override
    @Transactional
    public boolean logoutUser(Long userIdx) {
        boolean result = false;
        Optional<User> optionalUser = userRepository.findById(userIdx);
        if (optionalUser.isPresent()) {
            User logoutUser = optionalUser.get();
            logoutUser.setLogoutAt(getTimeStamp());
            userRepository.save(logoutUser);
            result = true;
        }
        return result;
    }

    @Override
    @Transactional
    public boolean loginUser(User user) {
        boolean result = false;
        User findUser = userRepository.findUserByUserId(user.getUserId());
        if (findUser != null && bCryptPasswordEncoder.matches(user.getPassword(), findUser.getPassword())) {
            findUser.setLoginAt(this.getTimeStamp());
            userRepository.save(findUser);
            result = true;
        }
        return result;
    }

    @Override
    @Transactional
    public boolean modifyUser(User user) {
        boolean result = false;
        Optional<User> optionalUser = userRepository.findById(user.getIdx());
        if (optionalUser.isPresent()) {
            User findUser = optionalUser.get();
            if (StringUtils.hasText(user.getPassword()))
                findUser.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            if (StringUtils.hasText(user.getMail()))
                findUser.setMail(user.getMail());
            if (user.getRole() != null)
                findUser.setRole(user.getRole());
            userRepository.save(findUser);
            result = true;
        }
        return result;
    }

    @Override
    @Transactional
    public Page<User> searchUsers(User searchUser, Pageable pageable) {
        Page<User> result = userRepository.findAll(PageRequest.of(pageable.getPageNumber() - 1, pageable.getPageSize(), pageable.getSort()));
        for (User user : result.getContent()){
            user.getTasks().size();
        }
        return result;
    }

    @Override
    @Transactional
    public boolean deleteUser(Long userIdx) {
        boolean result = false;
        Optional<User> optionalUser = userRepository.findById(userIdx);
        if (optionalUser.isPresent()) {
            User findUser = optionalUser.get();
            findUser.setClosed(true);
            findUser.setClosedAt(this.getTimeStamp());
            userRepository.save(findUser);
            result = true;
        }
        return result;
    }

    @Override
    public boolean isUserIdDuplicated(String userId) {
        User findUser = userRepository.findUserByUserId(userId);
        return findUser != null;
    }

    @Override
    @Transactional(readOnly = true)
    public User findUserByUserIdx(Long userIdx) {
        Optional<User> result = userRepository.findById(userIdx);
        User findUser = null;
        if (result.isPresent()) {
            findUser = result.get();
            //force lay loading
            findUser.getTasks().size();
        }
        return findUser;
    }

    private ZonedDateTime getTimeStamp() {
        return Instant.now().atZone(ZoneId.of("Asia/Seoul"));
    }
}
